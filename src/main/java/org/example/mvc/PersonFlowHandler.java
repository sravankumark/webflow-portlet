package org.example.mvc;

import org.springframework.stereotype.Component;
import org.springframework.webflow.mvc.portlet.AbstractFlowHandler;

@Component
public class PersonFlowHandler extends AbstractFlowHandler {
	public String getFlowId() {
		return "person";
	}
}
