package org.example.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.binding.message.MessageBuilder;
import org.springframework.binding.message.MessageContext;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
@Component("personBean")
@Entity
@Table(name="PERSON")
public class Person implements Serializable {

    private static final long serialVersionUID = -8712872385957386182L;
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="PERSON_ID")
    private Integer id;
    private String firstName = null;
    private String lastName = null;
    @OneToMany(fetch=FetchType.EAGER, cascade=CascadeType.ALL, orphanRemoval=true)
    @JoinColumn(name="PERSON_ID", nullable=false)
    //@Fetch(value=FetchMode.SUBSELECT)
    private Set<Address> addresses = null;
    @Temporal(value=TemporalType.TIMESTAMP)
    @Column(name = "CREATED_DATE", updatable = false,columnDefinition="timestamp default CURRENT_TIMESTAMP")
    private Date created = null;
    @Temporal(value=TemporalType.TIMESTAMP)
    @Column(name = "MODIFIED_DATE", insertable = false,columnDefinition="timestamp default NULL ON UPDATE CURRENT_TIMESTAMP")
    private Date modified=null;
   public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Set<Address> getAddresses() {
		return addresses;
	}

	public void setAddresses(Set<Address> addresses) {
		this.addresses = addresses;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getModified() {
		return modified;
	}

	public void setModified(Date modified) {
		this.modified = modified;
	}
	public Address findAddressById(Integer id) {
        Address result = null;
        if (addresses != null) {
            for (Address address : addresses) {
                if (address.getId().equals(id)) {
                    result = address;
                    break;
                }
            }
        }
        return result;
    }
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.getClass().getName() + "-");
        sb.append("  id=" + getId());
        sb.append("  firstName=" + firstName);
        sb.append("  lastName=" + lastName);
        sb.append("  addresses=[");
        if (addresses != null) {
            for (Address address : addresses) {
                sb.append(address.toString());
            }
        }
        sb.append("]");
        sb.append("  created=" + created);
        return sb.toString();
    }
    public void validatePersonForm(MessageContext context) {
        if (!StringUtils.hasText(firstName)) {
            context.addMessage(new MessageBuilder().error().source("firstName").code("person.form.firstName.error").build());
        }

        if (!StringUtils.hasText(lastName)) {
            context.addMessage(new MessageBuilder().error().source("lastName").code("person.form.lastName.error").build());
        }
    }

}
