package org.example.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.binding.message.MessageBuilder;
import org.springframework.binding.message.MessageContext;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
@Component("addressBean")
@Entity
@Table(name="ADDRESS")
public class Address implements Serializable{

    private static final long serialVersionUID = 7851794269407495684L;
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;
    
    @ManyToOne
    @JoinColumn(name = "PERSON_ID", insertable=false, updatable=false)
    private Person person;

    private String address = null;
    private String city = null;
    private String state = null;
    private String zipPostal = null;
    private String country = null;
    @Temporal(value=TemporalType.TIMESTAMP)
    @Column(name = "CREATED_DATE", updatable = false,columnDefinition="timestamp default CURRENT_TIMESTAMP")    
    private Date created = null;
    @Temporal(value=TemporalType.TIMESTAMP)
    @Column(name = "MODIFIED_DATE", insertable = false,columnDefinition="timestamp default NULL ON UPDATE CURRENT_TIMESTAMP")
    private Date modified=null;

   
    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZipPostal() {
		return zipPostal;
	}

	public void setZipPostal(String zipPostal) {
		this.zipPostal = zipPostal;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getModified() {
		return modified;
	}

	public void setModified(Date modified) {
		this.modified = modified;
	}

	@Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append(this.getClass().getName() + "-");
        sb.append("  id=" + getId());
        sb.append("  addresss=" + address);
        sb.append("  city=" + city);
        sb.append("  state=" + state);
        sb.append("  zipPostal=" + zipPostal);
        sb.append("  country=" + country);
        sb.append("  created=" + created);

        return sb.toString();
    }
	@Override
	public boolean equals(Object object){
		if(object instanceof Address) {
			Address address =(Address)object;
			if(address.getId().equals(this.getId())) {
				return true;
			}
		}
		return false;		
	}
	@Override
	public int hashCode() {
		return (id==null)?id:0;
	}
    public void validateAddressForm(MessageContext context) {
        if (!StringUtils.hasText(address)) {
            context.addMessage(new MessageBuilder().error().source("address").code("address.form.address.error").build());
        }
        
        if (!StringUtils.hasText(city)) {
            context.addMessage(new MessageBuilder().error().source("city").code("address.form.city.error").build());
        }
        
        if (!StringUtils.hasText(state)) {
            context.addMessage(new MessageBuilder().error().source("state").code("address.form.state.error").build());
        }
        
        if (!StringUtils.hasText(zipPostal)) {
            context.addMessage(new MessageBuilder().error().source("zipPostal").code("address.form.zipPostal.error").build());
        }
        
        if (!StringUtils.hasText(country)) {
            context.addMessage(new MessageBuilder().error().source("country").code("address.form.country.error").build());
        }
    }    
}