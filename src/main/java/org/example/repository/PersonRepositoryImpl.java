package org.example.repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.example.entity.Person;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
@Repository(value="personRepository")
public class PersonRepositoryImpl implements PersonRepository {
	@Autowired
	private SessionFactory factory;
	private Session session=null;
	@SuppressWarnings("unchecked")
	@Override
	public Person findOne(Integer id) {
		Person person=null;
		List<Person> persons=new ArrayList<Person>();
		session=factory.getCurrentSession();
		persons=session.createCriteria(Person.class).add(Restrictions.eq("id", id)).list();
		if(persons.size() >0) {
			person=persons.get(0);
		}
		return person;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Person> findAll() {
		session=factory.getCurrentSession();
		List<Person> persons=new ArrayList<Person>();
		persons=session.createCriteria(Person.class).list();
		return persons;
	}

	@Override
	public Person saveAndFlush(Person person) {
		session=factory.getCurrentSession();
		session.saveOrUpdate(person);
		return person;
	}

	@Override
	public void delete(Integer id) {
		session=factory.getCurrentSession();
		Person person=findOne(id);
		session.delete(person);
	}

}
