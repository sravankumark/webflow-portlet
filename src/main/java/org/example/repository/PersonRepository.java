package org.example.repository;

import java.util.Collection;

import org.example.entity.Person;


public interface PersonRepository {

	Person findOne(Integer id);

	Collection<Person> findAll();

	Person saveAndFlush(Person person);

	void delete(Integer id);
    
}
