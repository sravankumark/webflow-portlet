package org.example.service;

import java.util.Collection;

import org.example.entity.Address;
import org.example.entity.Person;
import org.example.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("personService")
@Transactional(readOnly=true)
public class PersonServiceImpl implements PersonService {
	@Autowired
	@Qualifier("personRepository")
    private PersonRepository repository;
    
    @Override
    public Person findById(Integer id) {
    	System.out.println("id is "+id);
        return repository.findOne(id);
    }

    @Override
    public Collection<Person> find() {
        return repository.findAll();
    }

    @Override
    @Transactional(readOnly=false)
    public Person save(Person person) {
        return repository.saveAndFlush(person);
    }

    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id) {
        repository.delete(id);
    }

    @Override
    @Transactional(readOnly=false)
    public Person saveAddress(Integer id, Address address) {
        Person person = findById(id);

        if (person.getAddresses().contains(address)) {
            person.getAddresses().remove(address);
        }

        person.getAddresses().add(address);

        return save(person);
    }

    @Override
    @Transactional(readOnly=false)
    public Person deleteAddress(Integer id, Integer addressId) {
        Person person = findById(id);        
        Address address = new Address();
        address.setId(addressId);       

        if (person.getAddresses().contains(address)) {
            person.getAddresses().remove(address);
        }

        return save(person);
    }
    
}
