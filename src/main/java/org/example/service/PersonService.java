package org.example.service;

import java.util.Collection;

import org.example.entity.Address;
import org.example.entity.Person;

public interface PersonService {

    public Person findById(Integer id);

    public Collection<Person> find();

    public Person save(Person person);
    
    public void delete(Integer id);
    
    public Person saveAddress(Integer id, Address address);

    public Person deleteAddress(Integer id, Integer addressId);

}
