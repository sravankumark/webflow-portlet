<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<portlet:actionURL var="actionUrl">
	<portlet:param name="execution" value="${flowExecutionKey}" />
</portlet:actionURL>
<h1><liferay-ui:message key="address.form.title"/></h1>

<div id="messages">
    <c:if test="${not empty statusMessageKey}">
       <p><liferay-ui:message key="${statusMessageKey}"/></p>
    </c:if>
</div>

<form:form modelAttribute="address" action="${actionUrl}">
    <form:hidden path="id" />

    <fieldset>
        <div class="form-row">
            <label for="address"><liferay-ui:message key="address.form.address"/>:</label>
            <span class="input"><form:input path="address" /></span>
            <span class="formerror"><form:errors path="address" /></span>
        </div>       
        <div class="form-row">
            <label for="city"><liferay-ui:message key="address.form.city"/>:</label>
            <span class="input"><form:input path="city" /></span>
            <span class="formerror"><form:errors path="city" /></span>
        </div>       
        <div class="form-row">
            <label for="state"><liferay-ui:message key="address.form.state"/>:</label>
            <span class="input"><form:input path="state" /></span>
            <span class="formerror"><form:errors path="state" /></span>
        </div>       
        <div class="form-row">
            <label for="zipPostal"><liferay-ui:message key="address.form.zipPostal"/>:</label>
            <span class="input"><form:input path="zipPostal" /></span>
            <span class="formerror"><form:errors path="zipPostal" /></span>
        </div>       
        <div class="form-row">
            <label for="country"><liferay-ui:message key="address.form.country"/>:</label>
            <span class="input"><form:input path="country" /></span>
            <span class="formerror"><form:errors path="country" /></span>
        </div>       
        <div class="form-buttons">
            <div class="button">
                <input type="submit" id="save" name="_eventId_save" value="<liferay-ui:message key="button.save"/>"/>&#160;
                <input type="submit" name="_eventId_cancel" value="<liferay-ui:message key="button.cancel"/>"/>&#160;   
            </div>    
        </div>
    </fieldset>
</form:form>