<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<portlet:actionURL var="actionUrl">
	<portlet:param name="execution" value="${flowExecutionKey}" />
</portlet:actionURL>
<h1>
	<liferay-ui:message key="person.form.title" />
</h1>

<div id="messages">
	<c:if test="${not empty statusMessageKey}">
		<p>
			<liferay-ui:message key="${statusMessageKey}" />
		</p>
	</c:if>
</div>

<form:form modelAttribute="person" action="${actionUrl}">
	<form:hidden path="id" />

	<fieldset>
		<div class="form-row">
			<label for="firstName"><liferay-ui:message
					key="person.form.firstName" />:</label> <span class="input"><form:input
					path="firstName" /></span> <span class="formerror"><form:errors
					path="firstName" /></span>
		</div>
		<div class="form-row">
			<label for="lastName"><liferay-ui:message
					key="person.form.lastName" />:</label> <span class="input"><form:input
					path="lastName" /></span> <span class="formerror"><form:errors
					path="lastName" /></span>
		</div>
		<div class="form-buttons">
			<div class="button">
				<input type="submit" id="save" name="_eventId_save"
					value="<liferay-ui:message key="button.save"/>" />&#160; <input
					type="submit" name="_eventId_cancel"
					value="<liferay-ui:message key="button.cancel"/>" />&#160;
			</div>
		</div>
	</fieldset>
</form:form>

<c:if test="${person.id > 0}">
	<div style="clear: both;">
		<a href="${flowExecutionUrl}&_eventId=addAddress"><liferay-ui:message
				key="address.form.button.add" /></a>
	</div>

	<div>&nbsp;</div>

	<c:if test="${empty person.addresses}">
		<div>&nbsp;</div>
	</c:if>

	<c:if test="${not empty person.addresses}">
		<table class="search">
			<tr>
				<th><liferay-ui:message key="address.form.address" /></th>
				<th><liferay-ui:message key="address.form.city" /></th>
				<th><liferay-ui:message key="address.form.state" /></th>
				<th><liferay-ui:message key="address.form.zipPostal" /></th>
				<th><liferay-ui:message key="address.form.country" /></th>
			</tr>
			<c:forEach var="address" items="${person.addresses}">
				<portlet:actionURL var="editAddressUrl">
					<portlet:param name="execution" value="${flowExecutionKey}" />
					<portlet:param name="_eventId" value="editAddress"/>
					<portlet:param name="addressId" value="${address.id}"/>
				</portlet:actionURL>
				<portlet:actionURL var="deleteAddressUrl">
					<portlet:param name="execution" value="${flowExecutionKey}" />
					<portlet:param name="_eventId" value="deleteAddress"/>
					<portlet:param name="addressId" value="${address.id}"/>
				</portlet:actionURL>
				<tr>
					<td>${address.address}</td>
					<td>${address.city}</td>
					<td>${address.state}</td>
					<td>${address.zipPostal}</td>
					<td>${address.country}</td>
					<td><a href="${editAddressUrl}"><liferay-ui:message key="button.edit" /></a> 
						<a href="${deleteAddressUrl}"><liferay-ui:message key="button.delete" /></a>
					</td>
				</tr>
			</c:forEach>
		</table>
	</c:if>
</c:if>
