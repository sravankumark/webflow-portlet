<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<portlet:renderURL var="addPerson">
	<portlet:param name="execution" value="${flowExecutionKey}" />
	<portlet:param name="_eventId" value="add" />
</portlet:renderURL>
<html>
<body>
	<h1>
		<liferay-ui:message key="person.search.title" />
	</h1>
	<a href="${addPerson}"><liferay-ui:message key="person.form.add" /></a>
	<table class="search">
		<tr>
			<th><liferay-ui:message key="person.form.firstName" /></th>
			<th><liferay-ui:message key="person.form.lastName" /></th>
		</tr>
		<c:forEach var="person" items="${persons}" varStatus="status">
			<tr>
				<c:set var="personFormId" value="person${status.index}" />
				<portlet:renderURL var="editPerson">
					<portlet:param name="execution" value="${flowExecutionKey}" />
					<portlet:param name="_eventId" value="edit" />
					<portlet:param name="id" value="${person.id}"/>
				</portlet:renderURL>
				<td>${person.firstName}</td>
				<td>${person.lastName}</td>
				<td><a href="${editPerson}"><liferay-ui:message	key="button.edit" /></a></td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>